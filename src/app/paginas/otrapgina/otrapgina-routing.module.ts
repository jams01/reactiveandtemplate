import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OtrapginaPage } from './otrapgina.page';

const routes: Routes = [
  {
    path: '',
    component: OtrapginaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OtrapginaPageRoutingModule {}
