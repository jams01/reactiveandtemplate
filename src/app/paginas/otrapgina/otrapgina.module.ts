import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OtrapginaPageRoutingModule } from './otrapgina-routing.module';

import { OtrapginaPage } from './otrapgina.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OtrapginaPageRoutingModule
  ],
  declarations: [OtrapginaPage]
})
export class OtrapginaPageModule {}
