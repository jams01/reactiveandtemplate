import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { forbiddenNameValidator } from 'src/app/myvalidators.directive';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  loginForm: FormGroup;
  constructor(private alertCtrl: AlertController, private router: Router,
    private formBuilder: FormBuilder) {
      this.loginForm = this.formBuilder.group(
        {
          email: ['',[Validators.email, Validators.required ,Validators.minLength(5), Validators.maxLength(10)]],
          nombre: ['', [Validators.required, Validators.minLength(4), forbiddenNameValidator(/Jonathan/i)]],
          apellido: [''],
          password: ['', [Validators.required, Validators.minLength(6)]]
        }
      );
    }

    get emailcontroller(){
      return this.loginForm.controls.email;
    }
    get nombrecontroller(){
      return this.loginForm.controls.nombre;
    }



 
}
