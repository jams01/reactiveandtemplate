import { Directive } from '@angular/core';
import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

@Directive({
  selector: '[appMyvalidators]'
})
export class MyvalidatorsDirective {

  constructor() { }

}

export function forbiddenNameValidator(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
  const forbidden = nameRe.test(control.value);
  return forbidden ? {forbiddenName: {value: control.value}} : null;
  };
}

