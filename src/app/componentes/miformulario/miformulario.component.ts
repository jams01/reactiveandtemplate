import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-miformulario',
  templateUrl: './miformulario.component.html',
  styleUrls: ['./miformulario.component.scss'],
})
export class MiformularioComponent implements OnInit {
  @Input() label1;
  @Input() label2;
  @Input() label3;
  constructor() { }

  ngOnInit() {}

}
