import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'tabs',
    loadChildren: () => import('./paginas/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: '',
    loadChildren: () => import('./paginas/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'camino1',
    loadChildren: () => import('./paginas/tab1/tab1.module').then( m => m.Tab1PageModule)
  },
  {
    path: 'prueba2',
    loadChildren: () => import('./paginas/prueba2/prueba2.module').then( m => m.Prueba2PageModule)
  },
  {
    path: 'otrapgina',
    loadChildren: () => import('./paginas/otrapgina/otrapgina.module').then( m => m.OtrapginaPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
