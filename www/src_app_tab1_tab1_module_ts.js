"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_tab1_tab1_module_ts"],{

/***/ 2580:
/*!*********************************************!*\
  !*** ./src/app/tab1/tab1-routing.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab1PageRoutingModule": () => (/* binding */ Tab1PageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 124);
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tab1.page */ 6923);




const routes = [
    {
        path: '',
        component: _tab1_page__WEBPACK_IMPORTED_MODULE_0__.Tab1Page,
    }
];
let Tab1PageRoutingModule = class Tab1PageRoutingModule {
};
Tab1PageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
    })
], Tab1PageRoutingModule);



/***/ }),

/***/ 2168:
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.module.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab1PageModule": () => (/* binding */ Tab1PageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 4666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tab1.page */ 6923);
/* harmony import */ var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../explore-container/explore-container.module */ 581);
/* harmony import */ var _tab1_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tab1-routing.module */ 2580);








let Tab1PageModule = class Tab1PageModule {
};
Tab1PageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_common__WEBPACK_IMPORTED_MODULE_6__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormsModule,
            _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_1__.ExploreContainerComponentModule,
            _tab1_routing_module__WEBPACK_IMPORTED_MODULE_2__.Tab1PageRoutingModule
        ],
        declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_0__.Tab1Page]
    })
], Tab1PageModule);



/***/ }),

/***/ 6923:
/*!***********************************!*\
  !*** ./src/app/tab1/tab1.page.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab1Page": () => (/* binding */ Tab1Page)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _tab1_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tab1.page.html?ngResource */ 3852);
/* harmony import */ var _tab1_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tab1.page.scss?ngResource */ 8165);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 2560);




let Tab1Page = class Tab1Page {
    constructor() { }
    enviar(form1, dato2) {
        console.log(form1.name.value);
        console.log(form1.lastname.value);
        console.log(dato2.value);
    }
};
Tab1Page.ctorParameters = () => [];
Tab1Page = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-tab1',
        template: _tab1_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_tab1_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], Tab1Page);



/***/ }),

/***/ 8165:
/*!************************************************!*\
  !*** ./src/app/tab1/tab1.page.scss?ngResource ***!
  \************************************************/
/***/ ((module) => {

module.exports = ".contenidorojo {\n  font-size: 2em;\n  color: #ff0000;\n}\n\n.contenidoazul {\n  color: blue;\n  font-size: 4em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRhYjEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksY0FBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7RUFDQSxjQUFBO0FBQ0oiLCJmaWxlIjoidGFiMS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGVuaWRvcm9qb3tcbiAgICBmb250LXNpemU6IDJlbTtcbiAgICBjb2xvcjogI2ZmMDAwMDtcbn1cblxuLmNvbnRlbmlkb2F6dWx7XG4gICAgY29sb3I6IGJsdWU7IFxuICAgIGZvbnQtc2l6ZTogNGVtO1xufSJdfQ== */";

/***/ }),

/***/ 3852:
/*!************************************************!*\
  !*** ./src/app/tab1/tab1.page.html?ngResource ***!
  \************************************************/
/***/ ((module) => {

module.exports = "<ion-header [translucent]=\"true\">\n \n</ion-header>\n\n<ion-content [fullscreen]=\"true\">\n    <ion-card>\n        <ion-item>\n          <ion-icon name=\"pin\" slot=\"start\"></ion-icon>\n          <ion-label>ion-item in a card, icon left, button right</ion-label>\n          <ion-button fill=\"outline\" slot=\"end\">View</ion-button>\n        </ion-item>\n      \n        <ion-card-content>\n          This is content, without any paragraph or header tags,\n          within an ion-card-content element.\n        </ion-card-content>\n    </ion-card>\n    <ion-item>\n        <ion-icon name=\"pin\" slot=\"start\"></ion-icon>\n        <ion-label>ion-item in a card, icon left, button right</ion-label>\n        <ion-button fill=\"outline\" slot=\"end\">View</ion-button>\n      </ion-item>\n      <ion-card mode=\"md\" color=\"primary\" >\n        <ion-img [src]=\"'./assets/edificio.jpeg'\" ></ion-img>\n        <ion-card-header color=\"secondary\" translucent>\n          <ion-card-subtitle>Destination</ion-card-subtitle>\n          <ion-card-title>Madison, WI</ion-card-title>\n        </ion-card-header>\n        <ion-card-content mode=\"ios\">\n          Founded in 1829 on an isthmus between Lake Monona and Lake Mendota, Madison was named the capital of the\n          Wisconsin Territory in 1836.\n        </ion-card-content>\n      </ion-card>\n <ion-button strong color=\"primary\">Default</ion-button>\n      <ion-item>\n        <ion-label>Awesome Label</ion-label>\n        <ion-input #fuera name=\"fuera\" type=\"text\" placeholder=\"Awesome Input\"></ion-input>\n      </ion-item>\n    \n    <form  #formulario1 (ngSubmit)=\"enviar(formulario1, fuera)\">\n      <ion-item>\n        <ion-label>Nombre</ion-label>\n        <ion-input [value]=\"fuera.value\" [disabled]=\"fuera.value=='cancelar'\" name=\"name\" type=\"text\" placeholder=\"Awesome Input\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>Apellido</ion-label>\n        <ion-input [disabled]=\"fuera.value=='enviar'\" name=\"lastname\" type=\"text\" placeholder=\"Awesome Input\"></ion-input>\n      </ion-item>\n      <ion-button type=\"submit\" expand=\"block\" fill=\"clear\" shape=\"round\">\n        Enviar\n      </ion-button>\n    </form>\n    <ion-fab  vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n      <ion-fab-button color=\"warning\">\n        <ion-icon name=\"camera-outline\"></ion-icon>\n      </ion-fab-button>\n      <ion-fab-list side=\"start\">\n        <ion-fab-button><ion-icon name=\"logo-ionic\"></ion-icon></ion-fab-button>\n        <ion-fab-button><ion-icon name=\"logo-angular\"></ion-icon></ion-fab-button>\n      </ion-fab-list>\n      <ion-fab-list side=\"top\">\n        <ion-fab-button>\n          <ion-icon name=\"logo-ionic\"></ion-icon>\n        </ion-fab-button>\n        <ion-fab-button><ion-icon name=\"logo-angular\"></ion-icon></ion-fab-button>\n        <ion-fab-button><ion-icon name=\"logo-angular\"></ion-icon></ion-fab-button>\n        <ion-fab-button><ion-icon name=\"logo-angular\"></ion-icon></ion-fab-button>\n        <ion-fab-button><ion-icon name=\"logo-angular\"></ion-icon></ion-fab-button>\n      </ion-fab-list>\n    </ion-fab>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_tab1_tab1_module_ts.js.map